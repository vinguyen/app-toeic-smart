package vinv.techsaku.toeicsmart;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView contentQuestion, result;
    RadioButton keyA, keyB, keyC, keyD;
    Button btnSubmit;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        contentQuestion = findViewById(R.id.content_question);
        keyA = findViewById(R.id.keyA);
        keyB = findViewById(R.id.keyB);
        keyC = findViewById(R.id.keyC);
        keyD = findViewById(R.id.keyD);
        result = findViewById(R.id.result);
        btnSubmit = findViewById(R.id.btnSubmit);

        keyA.setOnCheckedChangeListener(listenerRadio);
        keyB.setOnCheckedChangeListener(listenerRadio);
        keyC.setOnCheckedChangeListener(listenerRadio);
        keyD.setOnCheckedChangeListener(listenerRadio);

        contentQuestion.setText("Dr. Braun will write-------letters only for interns who master every task expected of a junior copy editor.");
        keyA.setText("(A) recommends");
        keyB.setText("(B) recommendation");
        keyC.setText("(C) recommended");
        keyD.setText("(D) recommending");
        keyA.setChecked(false);
        keyB.setChecked(false);
        keyC.setChecked(false);
        keyD.setChecked(false);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result.setVisibility(View.VISIBLE);
                if (keyB.isChecked()) {
                    result.setText("Đúng");
                }
                else  {
                    result.setText("Sai");
                }
            }
        });

    }


    CompoundButton.OnCheckedChangeListener listenerRadio = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            btnSubmit.setEnabled(true);
        }
    };
}
